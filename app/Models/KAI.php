<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KAI extends Model
{
    use HasFactory;

    protected $table = 'jadwal_kereta';
    protected $guarded = ['id'];

}
