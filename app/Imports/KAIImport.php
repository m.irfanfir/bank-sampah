<?php

namespace App\Imports;

use App\Models\KAI;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DB;
use Carbon\Carbon;

class KAIImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new KAI([
            'no_ka'                 => $row['no_ka'],
            'nama_kereta'           => $row['nama_kereta'],
            'stasiun_berangkat'     => $row['stasiun_berangkat'],
            'waktu_berangkat'       => $row['waktu_berangkat'],
            'stasiun_tiba'          => $row['stasiun_datang'],
            'waktu_tiba'            => $row['waktu_tiba'],
            'waktu_tempuh'          => $row['waktu_tempuh'],
            'created_at'            => Carbon::now(),
            'created_at'            => Carbon::now(),
        ]);

    }
}
