<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Models\User;
use DB;
use Auth;

class DashboardController extends Controller
{

    public function Dashboard(Request $request)
    {

        $auth = Auth::user();
        $user = DB::select("
                SELECT u.*, r.roles FROM users u 
                join roles as r on u.roles_id = r.id
                WHERE u.id = ".$auth->id."
            ")[0];

        $transaksi_sampah = DB::table('transaksi_sampah as ts')
            ->select('ms.nama_sampah')
            ->join('master_sampah as ms', 'ts.id_sampah', '=', 'ms.id')
            ->orderby('ms.created_at', 'desc')->get();
        
        return view('dashboard', compact('user', 'transaksi_sampah'));
    }
}
