<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;

class SampahController extends Controller
{
    public function index()
    {
        $jenis_sampah = DB::table('master_jenis_sampah')->orderby('created_at', 'DESC')->get();

        $sampah = DB::table('master_sampah as ms')
            ->select('ms.*', 'mjs.jenis_sampah')
            ->join('master_jenis_sampah as mjs', 'ms.id_jenis_sampah', '=', 'mjs.id')
            ->orderby('ms.created_at', 'DESC')->get();

        return view('sampah.index', compact('sampah', 'jenis_sampah'));
    }

    public function insert(Request $request)
    {
        $foto = base64_encode(file_get_contents($request->foto));

        DB::table('master_sampah')->insert([
            'id_jenis_sampah'   => $request->jenis_sampah,
            'nama_sampah'       => $request->nama_sampah,
            'deskripsi'         => $request->deskripsi,
            'foto'              => $foto,
            'harga'             => $request->harga,
            'created_at'        => Carbon::now(),
            'updated_at'        => Carbon::now(),
        ]);

        return redirect()->back()->with("success", 'Insert Successfully');
    }

    public function update(Request $request, $id)
    {
        if ($request->foto == null) {
            DB::table('master_sampah')->where('id', $id)->update([
                'id_jenis_sampah'   => $request->jenis_sampah,
                'nama_sampah'       => $request->nama_sampah,
                'deskripsi'         => $request->deskripsi,
                'harga'             => $request->harga,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]);
        }else{
            $foto = base64_encode(file_get_contents($request->foto));

            DB::table('master_sampah')->where('id', $id)->update([
                'id_jenis_sampah'   => $request->jenis_sampah,
                'nama_sampah'       => $request->nama_sampah,
                'deskripsi'         => $request->deskripsi,
                'foto'              => $foto,
                'harga'             => $request->harga,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]);
        }

        return redirect()->back()->with("success", 'Insert Successfully');
    }

    public function delete($id)
    {
        DB::table('master_sampah')->where('id', $id)->delete();

        return redirect()->back()->with("success", 'Delete Successfully');
    }
}
