<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;
use PDF;

class TransaksiController extends Controller
{
    public function index()
    {
        $barang = DB::table('barang')->orderby('created_at', 'desc')->get();
        $keranjang = DB::table('keranjang as k')->select('k.*', 'b.nama_barang', 'b.harga')
            ->join('barang as b', 'k.barang_id', '=', 'b.id')
            ->where('status', 1)
            ->orderby('k.created_at', 'DESC')->get();

        $total = $keranjang->sum('jumlah');

        $transaksi = DB::table('transaksi as t')->orderby('t.created_at', 'DESC')->get();

        return view('transaksi.index', compact('barang', 'keranjang', 'total', 'transaksi'));
    }

    public function add_keranjang(Request $request)
    {
        DB::beginTransaction();
            try {

            DB::table('keranjang')->insert([
                'barang_id'     => $request->barang,
                'qty'           => $request->qty,
                'jumlah'        => str_replace(',', '', $request->harga) * $request->qty,
                'status'        => 1,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);

            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Insert Successfully');
    }

    public function cek_harga(Request $request)
    {
        $barang = $request->input('barang');

        $harga = DB::table('barang')->where('id', $barang)->first();
        // dd($harga->harga);
        return response()->json(['harga' => $harga]);
    }

    public function keranjang_delete($id)
    {
        DB::table('keranjang')->where('id', $id)->delete();

        return redirect()->back()->with("success", 'Deleted Successfully');
    }

    public function add_transaksi(Request $request)
    {
        DB::beginTransaction();
            try {

            $date = date("Ymd");
            $int = rand(00000, 99999);
            $kode_transaksi = $date .''. $int;

            DB::table('transaksi')->insert([
                'kode_transaksi'    => $kode_transaksi,
                'total'             => $request->total,
                'jumlah_bayar'      => str_replace('.', '', $request->jml_bayar),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]);

            DB::table('keranjang')->where('status', 1)->update([
                'kode_transaksi'    => $kode_transaksi,
                'status'            => 0,
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ]);

            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Successfully');
    }

    public function print(Request $request, $id)
    {
        $user = Auth::user();
        $transaksi = DB::table('transaksi')->where('id', $id)->first();
        $keranjang = DB::table('keranjang as k')->select('k.*', 'b.nama_barang', 'b.harga')
            ->join('barang as b', 'k.barang_id', '=', 'b.id')
            ->where('kode_transaksi', $transaksi->kode_transaksi)->get();
        
        $pdf = PDF::loadView('pdf.transaksi', compact('transaksi', 'keranjang', 'user'))->setOptions(['defaultFont' => 'Courier-New']);
        // return view('pdf.transaksi');
        return $pdf->download('Transaksi.pdf');
        // return view('transaksi.print', compact('transaksi', 'keranjang', 'user'));
    }
}
