<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;

class TransaksiSampahController extends Controller
{
    public function transaksi(Request $request)
    {
        DB::table('transaksi_sampah')->insert([
            'id_sampah' => $request->sampah,
            'kg'        => $request->kg,
            'jumlah'    => str_replace(',', '', $request->harga) * $request->kg,
        ]);

        return redirect()->back()->with("success", 'Insert Successfully');
    }
}
