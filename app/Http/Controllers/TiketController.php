<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\KAIImport;

class TiketController extends Controller
{
    public function index()
    {
        $daerah_stasiun = DB::table('daerah_stasiun')->orderby('id', 'ASC')->get();
        $stasiun = DB::table('stasiun')->get();

        return view('tiket.index', compact('daerah_stasiun', 'stasiun'));
    }

    public function booking(Request $request)
    {
        $stasiun_awal = DB::table('stasiun')->where('kode_stasiun', $request->stasiun_awal)->first();
        $stasiun_tujuan = DB::table('stasiun')->where('kode_stasiun', $request->stasiun_tujuan)->first();

        $stasiun = [
            'stasiun_awal'      => $stasiun_awal->nama_stasiun,
            'stasiun_tujuan'    => $stasiun_tujuan->nama_stasiun
        ];

        $data = [
            'stasiun_awal' => $request->stasiun_awal,
            'stasiun_tujuan' => $request->stasiun_tujuan,
            'tanggal_berangkat' => $request->tanggal_berangkat,
            'dewasa' => $request->dewasa,
            'infant' => $request->infant
        ];
        $stasiun1 = DB::table('kereta')->get();
        
        return view('tiket.list_tiket', compact('data', 'stasiun', 'stasiun1'));
    }

    public function import(Request $request)
    {
        // dd(1);
        $data = $request->file('file');

        $nama_file = $data->getClientOriginalName();
        $data->move('KAI', $nama_file);

        Excel::import(new KAIImport, \public_path('/KAI/' . $nama_file));
        return redirect()->back();
    }
}
