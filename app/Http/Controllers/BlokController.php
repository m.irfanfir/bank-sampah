<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;

class BlokController extends Controller
{
    public function index()
    {
        $query = DB::table('blok')->orderby('blok', 'ASC')->get();

        return view('blok.index', compact('query'));
    }

    public function insert(Request $request)
    {
        DB::beginTransaction();
            try {

            DB::table('blok')->insert([
                'blok'          => ucwords($request->blok),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);

            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Insert Successfully');
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
            try {
            
            DB::table('blok')->where('id', $id)->update([
                'blok'          => ucwords($request->blok),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);

            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Update Successfully');
    }

    public function delete($id)
    {
        DB::table('blok')->where('id', $id)->delete();

        return redirect()->back()->with("success", 'Delete Successfully');
    }
}
