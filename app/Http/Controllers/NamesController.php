<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;
use PDF;

class NamesController extends Controller
{
    public function index(Request $request)
    {
        $nama = $request->nama;
        $alamat = $request->alamat;

        $blok = DB::table('blok')->orderBy('blok', 'ASC')->get();
        
        $query = DB::table('names as n')->select('n.*', 'b.blok')
        ->join('blok as b', 'n.id_blok', '=', 'b.id');

        if ($nama != null) {
            $query = $query->whereRaw('LOWER(n.nama) like LOWER(\'%'.$nama.'%\')');
        }

        if ($alamat != null) {
            $query = $query->where('n.id_blok', $alamat);
        }

        $query = $query->orderby('n.created_at', 'desc')->get();

        return view('names.index', compact('query', 'blok', 'nama', 'alamat'));
    }

    public function insert(Request $request)
    {
        DB::beginTransaction();
            try {

            DB::table('names')->insert([
                'nama'          => ucwords($request->nama),
                'id_blok'       => ucwords($request->blok),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);

            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Insert Successfully');
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
            try {
            
            DB::table('names')->where('id', $id)->update([
                'nama'          => ucwords($request->nama),
                'id_blok'       => ucwords($request->blok),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);

            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Update Successfully');
    }

    public function delete($id)
    {
        DB::table('names')->where('id', $id)->delete();

        return redirect()->back()->with("success", 'Delete Successfully');
    }

    public function pdf()
    {
        $data = DB::table('names')->orderby('id_blok', 'ASC')->get();

        $pdf = PDF::loadView('pdf.names_pdf', compact('data'))->setOptions(['defaultFont' => 'sans-serif']);
        // return view('pdf.names_pdf', compact('data'));
        return $pdf->download('nama.pdf');
    }
}
