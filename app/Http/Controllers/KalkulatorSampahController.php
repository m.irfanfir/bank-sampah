<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;

class KalkulatorSampahController extends Controller
{
    public function index()
    {
        $sampah = DB::table('master_sampah')->orderby('created_at', 'DESC')->get();

        $transaksi_sampah = DB::table('transaksi_sampah as ts')
            ->select('ts.*', 'ms.nama_sampah', 'ms.harga')
            ->join('master_sampah as ms', 'ts.id_sampah', '=', 'ms.id')
            ->orderby('created_at', 'desc')->get();

        return view('kalkulator_sampah.index', compact('sampah', 'transaksi_sampah'));
    }

    public function cek_harga(Request $request)
    {
        $sampah = $request->input('sampah');
        
        $harga = DB::table('master_sampah')->where('id', $sampah)->first();

        return response()->json(['harga' => $harga]);
    }
}
