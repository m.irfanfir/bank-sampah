<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Auth;

class BarangController extends Controller
{
    public function lang(Request $request)
    {
        //membuat session bahasa, secara default menggunakan english
        session()->put('language', Request('language') ?? 'en');

        return view('lang');
    }

    public function Barang(Request $request)
    {
        $nama_barang = $request->get('nama_barang');
        $query = DB::table('barang');
        if($nama_barang != null){
            $query = $query->whereRaw('LOWER(nama_barang) like LOWER(\'%'.$nama_barang.'%\')');
        }
        $query = $query->orderby('created_at', 'desc')->paginate(12);

        return view('barang.barang', compact('query', 'nama_barang'));
    }

    public function Update(Request $request, $id)
    {
        DB::beginTransaction();
            try {

                if($request->gambar != null){
                    $gambar = base64_encode(file_get_contents($request->gambar));
                    
                    DB::table('barang')->where('id', $id)->update([
                        'nama_barang'   => $request->nama_barang,
                        'harga'         => $request->harga,
                        'gambar'        => $gambar,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);
                }else{
                    DB::table('barang')->where('id', $id)->update([
                        'nama_barang'   => $request->nama_barang,
                        'harga'         => $request->harga,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);
                }

        DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Update Successfully');
    }

    public function Delete($id)
    {
        DB::table('barang')->where('id', $id)->delete();
        return redirect()->back()->with("success", 'Deleted');
    }

    public function Add(Request $request)
    {
        DB::beginTransaction();
            try {

                if($request->gambar != null){
                    $gambar = base64_encode(file_get_contents($request->gambar));
                    
                    DB::table('barang')->insert([
                        'nama_barang'   => $request->nama_barang,
                        'harga'         => $request->harga,
                        'gambar'        => $gambar,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);
                }else{
                    DB::table('barang')->insert([
                        'nama_barang'   => $request->nama_barang,
                        'harga'         => $request->harga,
                        'created_at'    => Carbon::now(),
                        'updated_at'    => Carbon::now(),
                    ]);
                }

        DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", 'Barang berhasil ditambahkan');
    }

    public function beli(Request $request, $id)
    {
        DB::beginTransaction();
            try {

                $fDate = Carbon::now()->format('d-m-Y');
                $exp = explode('-', $fDate);
                $imp = implode("", $exp);

                $length = 7;
                $rand = substr(str_shuffle('0123456789'),1,$length);

                $nomor_transaksi = $imp.''.$rand;

                DB::table('transaksi')->insert([
                    'nomor_transaksi'   => $nomor_transaksi,
                    'barang_id'         => $request->id_barang,
                    'users_id'          => Auth::user()->id,
                    'qty'               => $request->qty,
                    'jumlah'            => $request->jumlah,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ]);

        DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        return $e->getMessage();
        }

        return redirect()->back()->with("success", '');
    }
}
