<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;

class JenisSampahController extends Controller
{
    public function index()
    {
        $jenis_sampah = DB::table('master_jenis_sampah')->orderby('created_at', 'DESC')->get();

        return view('jenis_sampah.index', compact('jenis_sampah'));
    }

    public function insert(Request $request)
    {
        DB::table('master_jenis_sampah')->insert([
            'jenis_sampah'  => $request->jenis_sampah,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        return redirect()->back()->with("success", 'Insert Successfully');
    }

    public function update(Request $request, $id)
    {
        DB::table('master_jenis_sampah')->where('id', $id)->update([
            'jenis_sampah'  => $request->jenis_sampah,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now(),
        ]);

        return redirect()->back()->with("success", 'Update Successfully');
    }

    public function delete($id)
    {
        DB::table('master_jenis_sampah')->where('id', $id)->delete();

        return redirect()->back()->with("success", 'Delete Successfully');
    }
}
