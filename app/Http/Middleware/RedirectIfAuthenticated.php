<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Models\User;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $role = User::select('users.name', 'users.email', 'users.roles_id', 'roles.roles')
                    ->leftjoin('roles', 'users.roles_id', '=', 'roles.id')
                    ->first();
            // $sess = Session::put('role', $role->role);
            
            switch ($role->roles){
                case "Super Admin":
                    return redirect('/dashboard');
                    break;
                case "Admin":
                    return redirect('/dashboard');
                    break;
                case "User":
                    return redirect('/dashboard');
                    break;
            }
        }

        return $next($request);
    }
}
