-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2023 at 05:21 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank_sampah`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_jenis_sampah`
--

CREATE TABLE `master_jenis_sampah` (
  `id` int(10) NOT NULL,
  `jenis_sampah` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `master_sampah`
--

CREATE TABLE `master_sampah` (
  `id` int(10) NOT NULL,
  `id_jenis_sampah` int(10) DEFAULT NULL,
  `nama_sampah` varchar(100) DEFAULT NULL,
  `deskripsi` longtext DEFAULT NULL,
  `foto` longtext DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) NOT NULL,
  `roles` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2022-12-23 06:20:10', '2022-12-23 06:20:11'),
(2, 'Admin', '2022-12-23 06:20:12', '2022-12-23 06:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_id` int(10) NOT NULL DEFAULT 0,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `roles_id`, `email`, `phone_number`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(14, 'irfan', '$2a$12$kTsNETPW4Ii95qD1LtYUq.PFIysbigq7scuFNY94NUFXRgko2AQum', 'Irfan', 1, 'irfan@gmail.com', '081081081081', '2022-12-21 02:30:26', NULL, '2022-12-21 02:30:27', '2022-12-21 02:30:28'),
(15, 'super admin', '$2a$12$kTsNETPW4Ii95qD1LtYUq.PFIysbigq7scuFNY94NUFXRgko2AQum', 'Super Admin', 1, 'super_admin@gmail.com', '000000000000', '2022-12-23 06:22:13', NULL, '2022-12-23 06:22:15', '2022-12-23 06:22:15'),
(18, 'admin', '$2a$12$kTsNETPW4Ii95qD1LtYUq.PFIysbigq7scuFNY94NUFXRgko2AQum', 'Admin', 2, 'admin@gmail.com', '000000000001', '2022-12-23 06:22:30', NULL, '2022-12-23 06:22:32', NULL),
(19, 'test', '$2y$10$c3UKpl3UuKFuqOTQLg8/Cu.FNGU0/p4oiAcoQxv6qpGA/15WSe7CO', 'Admin', 1, 'asasasas@gmail.com', '089089089089', NULL, NULL, '2023-09-11 02:44:22', '2023-09-11 02:44:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_jenis_sampah`
--
ALTER TABLE `master_jenis_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_sampah`
--
ALTER TABLE `master_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_number_unique` (`phone_number`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_jenis_sampah`
--
ALTER TABLE `master_jenis_sampah`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_sampah`
--
ALTER TABLE `master_sampah`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
