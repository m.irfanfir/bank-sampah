<!-- jQuery -->
<script src="{{ URL::asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('dist/js/adminlte.min.js') }}"></script>
<script src="{{ URL::asset('js/swal.js')}}"></script>

<script>
  	@if(Session::has('success'))
    	Swal.fire({
      		icon: 'success',
      		title: 'Berhasil...',
      		text: "{{ Session::get('success') }}",
    	})
  	@endif
   	
   	@if(Session::has('reject'))
    		Swal.fire({
	      		icon: 'reject',
			    title: 'Reject Berhasil',
			    text: "{{ Session::get('reject') }}",
    		})
  	@endif

  	@if(Session::has('info'))
    		Swal.fire({
		      	icon: 'info',
		      	title: 'Info',
		      	text: "{{ Session::get('info') }}",
    		})
  	@endif

  	@if(Session::has('warning'))
    	Swal.fire({
	      	icon: 'warnign',
	      	title: 'Warning...',
	      	text: "{{ Session::get('warning') }}",
    	})
  	@endif

  	@if(Session::has('error'))
    	Swal.fire({
      		icon: 'error',
      		title: 'Gagal...',
      		text: "{{ Session::get('error') }}",
    	})
  	@endif
</script>