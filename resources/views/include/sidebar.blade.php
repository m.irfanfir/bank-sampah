<!-- Brand Logo -->
<a href="{{URL::asset('/dashboard')}}" class="brand-link">
  <div class="image">
    <img src="{{ URL::asset('dist/img/bank-sampah.png')}}"  alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
  </div>
  <span class="brand-text font-weight-light">BANK SAMPAH</span>
</a>
<?php
  $getRole = Auth::user()->get_role();
?>
<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      @if($getRole->roles_id == 2)
        <li class="nav-item">
          <a href="{{ URL::asset('/dashboard') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'dashboard') ) active @endif">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ URL::asset('/kalkulator-sampah') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'kalkulator') ) active @endif">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Kalkulator Sampah
            </p>
          </a>
        </li>
      @elseif($getRole->roles_id == 1)
        <li class="nav-item">
          <a href="{{ URL::asset('/dashboard') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'dashboard') ) active @endif">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ URL::asset('/sampah') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'sampah') ) active @endif">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Sampah
            </p>
          </a>
        </li>
      @endif
      <!-- <li class="nav-item">
        <a href="{{ URL::asset('/jenis-sampah') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'jenis') ) active @endif">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Jenis Sampah
          </p>
        </a>
      </li> -->
      <!-- <li class="nav-item">
        <a href="{{ URL::asset('/barang') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'barang') ) active @endif">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Barang
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ URL::asset('/blok/index') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'index') ) active @endif">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Blok
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ URL::asset('/names/index') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'names') ) active @endif">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Names
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ URL::asset('/transaksi/index') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'transaksi') ) active @endif">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Transaksi
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ URL::asset('/menus') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'menu') ) active @endif">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Menu
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ URL::asset('/tiket') }}" class="nav-link @if(str_contains(Route::currentRouteName(), 'tiket') ) active @endif">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Tiket
          </p>
        </a>
      </li> -->
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->