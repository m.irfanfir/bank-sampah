<?php
  $auth = Auth::user();
  $user = DB::select("
          SELECT u.*, r.roles FROM users u 
          join roles as r on u.roles_id = r.id
          WHERE u.id = ".$auth->id."
      ")[0];
?>
<!-- Left navbar links -->
<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
  </li>
  <li class="nav-item d-none d-sm-inline-block">
    <a href="#" class="nav-link"></a>
  </li>
</ul>

<!-- SEARCH FORM -->
<!-- <form class="form-inline ml-3">
  <div class="input-group input-group-sm">
    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
    <div class="input-group-append">
      <button class="btn btn-navbar" type="submit">
        <i class="fas fa-search"></i>
      </button>
    </div>
  </div>
</form> -->

<ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
  <li class="nav-item dropdown">
    <a id="dropdownSubMenu1" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
      <span>{{$user->name}} | {{$user->roles}}</span>
    </a>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
      <div class="dropdown-divider"></div>
      <a href="{{URL::asset('/ganti-password')}}" class="dropdown-item">Ganti Password</a>
      <div class="dropdown-divider"></div>
      <a href="{{URL::asset('/profile')}}" class="dropdown-item">Profile</a>
      <div class="dropdown-divider"></div>
      <a href="{{ route('logout') }}" class="dropdown-item"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          Logout
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
    </div>
  </li>
</ul>