<!-- jQuery -->
<script src="{{ URL::asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ URL::asset('dist/js/demo.js') }}"></script>
<!-- DataTables -->
<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('dist/js/sweetalert2.all.min.js') }}"></script>
<script src="{{ URL::asset('js/swal.js')}}"></script>
<script src="{{ URL::asset('js/icon.js')}}"></script>
<script src="{{ URL::asset('plugins/select2/js/select2.full.min.js')}}"></script>

<script src="{{ URL::asset('js/jquery.countdown.js')}}"></script>
<script src="{{ URL::asset('js/jquery.countdown.min.js')}}"></script>
<script src="{{ URL::asset('confirm/jquery-confirm.min.js') }}"></script>
<script src="{{ URL::asset('confirm/sweetalert.min.js') }}"></script>

<script src="{{ URL::asset('js/chart.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-toggle.min.js') }}"></script>
<!-- AdminLTE App -->
<script>
  	@if(Session::has('success'))
    	Swal.fire({
      		icon: 'success',
      		title: 'Success',
      		text: "{{ Session::get('success') }}",
    	})
  	@endif
   	
   	@if(Session::has('reject'))
    		Swal.fire({
	      		icon: 'reject',
			    title: 'Reject Berhasil',
			    text: "{{ Session::get('reject') }}",
    		})
  	@endif

  	@if(Session::has('info'))
    		Swal.fire({
		      	icon: 'info',
		      	title: 'Info',
		      	text: "{{ Session::get('info') }}",
    		})
  	@endif

  	@if(Session::has('warning'))
    	Swal.fire({
	      	icon: 'warnign',
	      	title: 'Warning...',
	      	text: "{{ Session::get('warning') }}",
    	})
  	@endif

  	@if(Session::has('error'))
    	Swal.fire({
      		icon: 'error',
      		title: 'Error...',
      		text: "{{ Session::get('error') }}",
    	})
  	@endif
</script>
