@extends('layouts.backend')
@section('content')

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>General Form</h1>
      </div> -->
      <!-- <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">General Form</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12">
        <div class="info-box">
          <span class="info-box-icon bg-default elevation-1"><i class="fas fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-number">Selamat Datang, {{$user->name}}</span>
            <span class="info-box-text">{{$user->roles}}</span>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">
              Rekapitulasi Jumlah Sampah
            </h3>
          </div>
          <div class="card-body">
            <div class="chart">
              <canvas id="rekapitulasi_perbulan" style="max-height: 250px; max-width: 100%;"></canvas>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
@section('jscustom')
<script>
  const rekapitulasi_perbulan = {
    labels: [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember',
    ],
    datasets: [{
      label: 'My First Dataset',
      data: [
              '1',
              '2',
              '3',
              '4',
              '5',
              '6',
              '7',
              '8',
              '9',
              '10',
              '11',
              '12',
            ],
      backgroundColor: [
        '#FFFF00',
        '#228B22',
        '#8A2BE2',
        '#A52A2A',
        '#FF4500',
        '#D2691E',
        '#DC143C',
        '#00008B',
        '#B8860B',
        '#006400',
        '#FF1493',
        '#00FF00',
      ],
      hoverOffset: 4
    }]
  };
  
  const config1 = {
    type: 'doughnut',
    data: rekapitulasi_perbulan,
    options: {}
  };

  const ChartStatusUser = new Chart(
    document.getElementById('rekapitulasi_perbulan'),
    config1
  );
</script>
<script>
  $('.select2').select2()

  //Initialize Select2 Elements
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
</script>
@endsection