@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>DataTables</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DataTables</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Pemesanan Tiket</h3>
          </div>
          <form action="{{ url('/booking/tiket')}}" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Disabled Result</label>
                    <select class="form-control select2bs4" name="stasiun_awal" id="stasiun_awal" required>
                      <option value="">Pilih Stasiun</option>
                      @foreach ($daerah_stasiun as $ds)
                          <option disabled="disabled">{{$ds->nama_daerah}}</option>
                          @foreach ($stasiun as $st)
                            @if($st->id_daerah_stasiun == $ds->id)
                              <option value="{{$st->kode_stasiun}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$st->nama_stasiun}} ({{$st->kode_stasiun}})</option>
                            @endif
                          @endforeach
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Stasiun Tujuan</label>
                    <select class="form-control select2bs4" name="stasiun_tujuan" id="stasiun_tujuan" required>
                      <option value="">Pilih Stasiun</option>
                      @foreach ($daerah_stasiun as $ds)
                          <option disabled="disabled">{{$ds->nama_daerah}}</option>
                          @foreach ($stasiun as $st)
                            @if($st->id_daerah_stasiun == $ds->id)
                              <option value="{{$st->kode_stasiun}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$st->nama_stasiun}} ({{$st->kode_stasiun}})</option>
                            @endif
                          @endforeach
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputPassword1">Tanggal Berangkat</label>
                  <input type="date" name="tanggal_berangkat" class="form-control" required>
                </div>
                <div class="col-md-4">
                  <label for="exampleInputPassword1">Dewasa</label>
                  <select class="form-control select2bs4" name="dewasa" id="dewasa" required>
                    <option value="">Dewasa (>= 3 Tahun)</option>
                    <option value="1">1 Dewasa</option>
                    <option value="2">2 Dewasa</option>
                    <option value="3">3 Dewasa</option>
                    <option value="4">4 Dewasa</option>
                    <option value="5">5 Dewasa</option>
                    <option value="6">6 Dewasa</option>
                    <option value="7">7 Dewasa</option>
                    <option value="8">8 Dewasa</option>
                    <option value="9">9 Dewasa</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <label for="exampleInputPassword1">Infant</label>
                  <select class="form-control select2bs4" name="infant" id="infant">
                    <option value="">Infant (<= 3 Tahun)</option>
                    <option value="1">1 Infant</option>
                    <option value="2">2 Infant</option>
                    <option value="3">3 Infant</option>
                    <option value="4">4 Infant</option>
                    <option value="5">5 Infant</option>
                    <option value="6">6 Infant</option>
                    <option value="7">7 Infant</option>
                    <option value="8">8 Infant</option>
                    <option value="9">9 Infant</option>
                  </select>
                </div>
              </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Pemesanan Tiket</h3>
          </div>
          <form action="{{ url('/import')}}" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>File</label>
                    <input type="file" name="file">
                  </div>
                </div>
              </div>
            </div>

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section> -->
@stop
@section('jscustom')
<script>
  $('.select2').select2()

  //Initialize Select2 Elements
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
</script>
@endsection