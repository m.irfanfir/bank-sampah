@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>DataTables</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DataTables</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-6">
        <div class="small-box bg-primary">
          <div class="inner">
            <h3>{{$stasiun['stasiun_awal']}} <i class="fa-solid fa-arrow-right"></i> {{$stasiun['stasiun_tujuan']}}</h3>
            <p>
              {{\Carbon\Carbon::parse($data['tanggal_berangkat'])->isoFormat('dddd, D MMMM Y')}} - {{$data['dewasa']}} Dewasa
              @if($data['infant'] != 0)
                - {{$data['infant']}} Bayi
              @endif
            </p>
            <a href="{{ url('/tiket')}}" class="btn btn-default btn-sm">Ganti Pencarian</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-12">
          <div class="info-box-content">
            <div class="row">
              <div class="col-md-3">
                <div class="user-block">
                  <strong>KERETA</strong>
                </div>
              </div>
              <div class="col-md-2">
                <div class="user-block">
                  <strong>BERANGKAT</strong>
                </div>
              </div>
              <div class="col-md-2">
                <div class="user-block">
                  <strong>DURASI</strong>
                </div>
              </div>
              <div class="col-md-2">
                <div class="user-block">
                  <strong>TIBA</strong>
                </div>
              </div>
              <div class="col-md-3">
                <div class="user-block">
                  <strong>HARGA</strong>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      @foreach($stasiun1 as $st)
        <div class="col-md-12 col-sm-12 col-12">
          <div class="info-box bg-navy color-palette">
            <div class="info-box-content">
              <div class="row">
                <div class="col-md-3">
                  <div class="user-block">
                    <span>
                      <a>{{$st->nama_kereta}}</a>
                    </span>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="user-block">
                    <span>
                      <a>{{$stasiun['stasiun_awal']}}</a>
                    </span>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="user-block">
                    <span>
                      <i class="fa-solid fa-arrow-right"></i>
                    </span>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="user-block">
                    <span>
                      <a>{{$stasiun['stasiun_tujuan']}}</a>
                    </span>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="user-block">
                    <span>
                      <a>Jonathan Burke Jr.</a>
                    </span><br>
                    <a href="{{ url('/names/delete')}}" class="btn btn-default btn-sm"><strong>PESAN</strong></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</section>
@stop
@section('jscustom')
<script>
  $('.select2').select2()

  //Initialize Select2 Elements
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
</script>
@endsection