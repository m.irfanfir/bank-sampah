@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>DataTables</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DataTables</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Blok</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <p>
              <a title="Tambah Blok" type="button" href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_blok"><i class="fas fa-plus"></i></a>
            </p>
            <table id="blok" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Blok</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              @foreach($query as $index => $blok)
                <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$blok->blok}}</td>
                  <td class="py-0 align-middle">
                    <div class="btn-group btn-group-sm">
                      <a title="Update Blok" type="button" href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#update_blok"><i class="fas fa-eye"></i></a>
                      <a href="{{ url('/blok/delete/'.$blok->id)}}" title="Delete" class="btn btn-default btn-sm button delete-blok"><i class="fas fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
@include('blok.modal')
@stop
@section('jscustom')
<script>
  $(function () {
    $("#blok").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<script>
  $('.delete-blok').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Data akan dihapus?',
          text: '',
          icon: 'warning',
          buttons: ["Tidak", "Ya!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }else{
            swal("Delete dibatalkan", "", "error");
          }
      });
  });
</script>
@endsection