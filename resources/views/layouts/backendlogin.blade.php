<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Login / Signup</title>
  <link rel="stylesheet" href="{{ URL::asset('dist/css/stylelogin.css') }}">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="form-structor">
  @yield('content')
</div>
<!-- partial -->
<script  src="{{ URL::asset('dist/js/scriptlogin.js') }}"></script>
</body>
</html>
