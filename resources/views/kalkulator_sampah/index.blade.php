@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>DataTables</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DataTables</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-4">
        <!-- general form elements -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Kalkulator Sampah</h3>
          </div>
          <form action="{{ url('/transaksi')}}" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Jenis Sampah</label>
                <select class="form-control select2bs4" name="sampah" required id="sampah">
                    <option value="">Pilih Barang</option>
                    @foreach ($sampah as $js)
                        <option value="{{$js->id}}">{{$js->nama_sampah}}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Harga</label>
                <input type="text" class="form-control" id="harga" name="harga" readonly placeholder="Harga">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Kg</label>
                <input type="number" class="form-control" id="kg" min="1" step="1" value="1" required name="kg" placeholder="Kg">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">List Transaksi Sampah</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table_transaksi" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Jenis Sampah</th>
                <th>Harga Per Kg</th>
                <th>Kg</th>
                <th>Jumlah</th>
              </tr>
              </thead>
              <tbody>
              @foreach($transaksi_sampah as $index => $ts)
                <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$ts->nama_sampah}}</td>
                  <td>{{number_format($ts->harga,0,',','.')}}</td>
                  <td>{{$ts->kg}}</td>
                  <td>{{number_format($ts->jumlah,0,',','.')}}</td>
                </tr>
              @endforeach
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
@stop
@section('jscustom')
<script>
  $(function () {
    $("#table_transaksi").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<script>
    $('#sampah').on('change', function(e) {
        cekHarga()
    });

    var paramsampah = {
            sampah : null
        };

    function cekHarga () {
        paramsampah.sampah = $('#sampah').val();

        $.get('{{url("/cek/harga")}}', paramsampah, function(data){
            console.log(data.harga.harga);
            $('#harga').val(new Intl.NumberFormat().format(data.harga.harga));
        })
    }
</script>
<script>
  $(function () {
    $("#keranjang").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    })
  });

  $(function () {
    $("#transaksi").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    })
  });
</script>
<script>
  $('.delete-keranjang').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Data akan dihapus?',
          text: '',
          icon: 'warning',
          buttons: ["Tidak", "Ya!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }else{
            swal("Delete dibatalkan", "", "error");
          }
      });
  });
</script>
<script>
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
</script>
@endsection