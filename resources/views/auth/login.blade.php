@extends('layouts.backendlogin')

@section('content')
<form method="POST" action="{{ route('login') }}">
    @csrf
  <div class="signup">
    <h2 class="form-title" id="signup">Login <span>or</span></h2>
    <div class="form-holder">
      <input type="email" class="input @error('email') is-invalid @enderror" name="email" required autocomplete="email" autofocus placeholder="Email" />
      @error('email')
          <span class="invalid-feedback" role="alert">
              <strong style="font-size: 10px;padding-left: 15px; color: red;">{{ $message }}</strong>
          </span>
      @enderror

      <input type="password" name="password" class="input @error('email') is-invalid @enderror" required autocomplete="email" placeholder="Password" />
      @error('password')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <button type="submit" class="submit-btn">Login</button>
  </div>
</form>
<form method="POST" action="{{ url('/register')}}">
  @csrf
  <div class="login slide-up">
    <div class="center">
      <h2 class="form-title" id="login"><span>or</span>Sign up</h2>
      <div class="form-holder">
        <input type="text" class="input" name="name" placeholder="Name" />
        <input type="email" class="input" name="email" placeholder="Email" />
        <input type="password" class="input" name="password" placeholder="Password" />
      </div>
      <button type="submit" class="submit-btn">Sign up</button>
    </div>
  </div>
</form>
@endsection