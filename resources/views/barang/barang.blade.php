@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>General Form</h1>
      </div> -->
      <!-- <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">General Form</li>
        </ol>
      </div> -->
    </div>
  </div>
</section>
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Barang</h3>
    </div>
    <div class="card-body">
      <p>
        <a title="Tambah Barang" type="button" href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_barang"><i class="fas fa-plus"></i></a>
      </p>
      <form>
        <div class="row">
          <div class="col-sm-5">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama Barang</label>
              <input type="text" class="form-control" name="nama_barang" id="nama_barang" value="{{$nama_barang}}" placeholder="Nama Barang" autocomplete="off">
            </div>
          </div>
          <div class="col-sm-5">
            <div class="form-group">
              <label for="exampleInputEmail1">Harga</label>
              <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga" autocomplete="off">
            </div>
          </div>
          <div class="col-sm-1">
            <div class="form-group">
              <label for="exampleInputEmail1">&nbsp;</label></br>
              <button type="submit" class="btn btn-default">Search</button>
            </div>
          </div>
          <div class="col-sm-1">
            <div class="form-group">
              <label for="exampleInputEmail1">&nbsp;</label></br>
              <a href="#" class="btn btn-default">Reset</a>
            </div>
          </div>
        </div>
      </form>
      <div class="row">
        @foreach($query as $barang)
          <div class="col-md-3">
            <div class="card card-default card-outline">
                <div class="card-body box-profile">
                  @if($barang->gambar)
                    <img style="height: 200px; width: 200px;" class="img-fluid" src="data:image;base64, {{$barang->gambar}}" alt="Photo">
                  @else
                    <img style="height: 200px; width: 200px;" class="img-fluid" src="{{ URL::asset('dist/img/image_not_found.png') }}" alt="Photo">
                  @endif
                  <h3 class="profile-username text-center">{{$barang->nama_barang}}</h3>

                  <p class="text-muted text-center">Rp. {{number_format($barang->harga,0,',','.')}}</p>
                  <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                      <b>Harga</b> <a class="float-right">Rp. 1.322.000.000</a>
                    </li>
                    <li class="list-group-item">
                      <b>Terjual</b> <a class="float-right">543</a>
                    </li>
                    <li class="list-group-item">
                      <a class="float-center">13,287</a>
                    </li>
                  </ul>
                  <div class="row">
                    <div class="col-md-4">
                      <a title="Update Barang" type="button" href="#" class="btn btn-default btn-block" data-toggle="modal" data-target="#update_barang-{{ $barang->id }}"><i class="fas fa-pencil"></i></a>
                    </div>
                    <div class="col-md-4">
                      <a title="Beli" href="#" class="btn btn-default btn-block"  data-toggle="modal" data-target="#beli-{{ $barang->id }}"><i class="fas fa-cart-shopping"></i></a>
                    </div>
                    <div class="col-md-4">
                      <a href="{{ url('/hapus-barang/'.$barang->id)}}" title="Hapus Barang" class="btn btn-default btn-block hapus_barang"><i class="fas fa-trash"></i></a>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        @endforeach
      </div>
      <div class="pagination pagination-sm">
        {!! $query->links('barang.pagination') !!}
      </div>
    </div>
  </div>
</section>
@include('barang.modal');

@stop
@section('jscustom')
<script>
  $('.hapus_barang').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Data akan dihapus?',
          text: '',
          icon: 'warning',
          buttons: ["Tidak", "Ya!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }else{
            swal("Aksi dibatalkan", "", "error");
          }
      });
  });
</script>
<script>
    $('#qty').on('change', function(e) {
        hitung()
    });

    function hitung () {
        var qty = $('#qty').val();
        var harga = $('#harga_barang').val();

        hasil = qty * harga;
        
        $('#jumlah').val(hasil);
    }
</script>
@endsection