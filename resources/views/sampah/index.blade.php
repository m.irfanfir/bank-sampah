@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>DataTables</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DataTables</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Sampah</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <p>
              <a title="Tambah Sampah" type="button" href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_sampah"><i class="fas fa-plus"></i></a>
            </p>
            <table id="table_sampah" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Nama Sampah</th>
                <th>Harga Per Kg</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              @foreach($sampah as $index => $js)
                <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$js->nama_sampah}}</td>
                  <td>{{number_format($js->harga,0,',','.')}}</td>
                  <td class="py-0 align-middle">
                    <div class="btn-group btn-group-sm">
                      <a title="Update Jenis Sampah" type="button" href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#update_sampah-{{ $js->id }}"><i class="fas fa-eye"></i></a>
                      <a href="{{ url('/sampah/delete/'.$js->id)}}" title="Delete" class="btn btn-default btn-sm button delete-sampah"><i class="fas fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
@include('sampah.modal')
@stop
@section('jscustom')
<script>
  $(function () {
    $("#table_sampah").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<script>
  $('.delete-sampah').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Data akan dihapus?',
          text: '',
          icon: 'warning',
          buttons: ["Tidak", "Ya!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }else{
            swal("Delete dibatalkan", "", "error");
          }
      });
  });
</script>
<script>
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
</script>
@endsection