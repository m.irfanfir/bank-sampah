@foreach($transaksi as $index => $t)
<?php
  $keranjang = DB::table('keranjang as k')->select('k.*', 'b.nama_barang', 'b.harga')
        ->join('barang as b', 'k.barang_id', '=', 'b.id')
        ->where('kode_transaksi', $t->kode_transaksi)
        ->get();
?>
<div class="modal fade" id="detail_transaksi-{{ $t->id }}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Transaksi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-group list-group-unbordered mb-3">
            <p class="lead">Tanggal Transaksi : {{\Carbon\Carbon::parse($t->created_at)->isoFormat('D MMMM Y')}}</p>
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-3">
                  <b>Nama Barang</b>
                </div>
                <div class="col-md-3">
                  <b class="float-right">Qty</b>
                </div>
                <div class="col-md-3">
                  <b class="float-right">Harga</b>
                </div>
                <div class="col-md-3">
                  <b class="float-right">Jumlah</b>
                </div>
              </div>
            </li>
          @foreach($keranjang as $index => $k)
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-3">
                  <b>{{$k->nama_barang}}</b>
                </div>
                <div class="col-md-3">
                  <a class="float-right">{{$k->qty}}</a>
                </div>
                <div class="col-md-3">
                  <a class="float-right">Rp. {{number_format($k->harga,0,',','.')}}</a>
                </div>
                <div class="col-md-3">
                  <a class="float-right">Rp. {{number_format($k->jumlah,0,',','.')}}</a>
                </div>
              </div>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
@endforeach