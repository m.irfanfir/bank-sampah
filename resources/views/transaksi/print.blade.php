<!DOCTYPE html>
<html lang="en" >
 
<head>
 
  <meta charset="UTF-8">
  <title>Invoice</title>
 
  <style>
@media print {
    .page-break { display: block; page-break-before: always; }
}
#invoice-POS {
/*  box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);*/
  padding: 2mm;
  margin: 0 auto;
  width: 58mm;
  background: #FFF;
}
#invoice-POS ::selection {
  background: #f31544;
  color: #FFF;
}
#invoice-POS ::moz-selection {
  background: #f31544;
  color: #FFF;
}
#invoice-POS h1 {
  font-size: 1.5em;
  color: #222;
}
#invoice-POS h2 {
  font-size: .9em;
}
#invoice-POS h3 {
  font-size: 1.2em;
  font-weight: 300;
  line-height: 2em;
}
#invoice-POS p {
  font-size: .7em;
  color: black;
  line-height: 1.2em;
}
#invoice-POS #top, #invoice-POS #mid, #invoice-POS #bot {
  /* Targets all id with 'col-' */
   solid #EEE;
}
#invoice-POS #top {
  min-height: 100px;
}
#invoice-POS #mid {
  min-height: 80px;
}
#invoice-POS #bot {
  min-height: 50px;
}/*
#invoice-POS #top .logo {
  height: 40px;
  width: 150px;
  background: url(https://www.sistemit.com/wp-content/uploads/2020/02/SISTEMITCOM-smlest.png) no-repeat;
  background-size: 150px 40px;
}*/
/*#invoice-POS .clientlogo {
  float: left;
  height: 60px;
  width: 60px;
  background: url(https://www.sistemit.com/wp-content/uploads/2020/02/SISTEMITCOM-smlest.png) no-repeat;
  background-size: 60px 60px;
  border-radius: 50px;
}*/
#invoice-POS .info {
  display: block;
  margin-left: 0;
  font-family: Courier New;
}
#invoice-POS .title {
  float: right;
}
#invoice-POS .title p {
  text-align: right;
}
#invoice-POS table {
  width: 100%;
  border-collapse: collapse;
}
#invoice-POS .tabletitle {
  font-size: .5em;
  background: #EEE;
}
#invoice-POS .service {
  border-bottom: 1px solid #EEE;
}
#invoice-POS .item {
  width: 24mm;
}
#invoice-POS .itemtext {
  font-size: .5em;
}
#invoice-POS #legalcopy {
  margin-top: 5mm;
}
 
    </style>
 
  <script>
  window.console = window.console || function(t) {};
</script>
 
 
 
  <script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
</script>
 
 
</head>
 
<body translate="no" >
 
 
  <div id="invoice-POS">
 
    <!-- <center id="top">
      <div class="logo"></div>
      <div class="info"> 
        <h2>SistemIT.com</h2>
      </div>
    </center> -->
 
    <div id="mid">
      <div class="info">
        <h2 style="text-align: center; font-size: 18px; margin-top: 0px; margin-bottom: 0px;">NOK OUTFIT</h2>
        <p style="margin-top: 0px; margin-bottom: 0px; text-align: center;">Jl. Gintung Lor Susukan Cirebon</p>
        <hr/ style="margin-top: 0px;">
        <table cellspacing='0' cellpadding='0' style='width:218px; font-size:11px; font-family:Courier New;  border-collapse: collapse;' border='0'>
          <tbody>
          <tr>
            <td class="textt">Kasir</td>
            <td>:</td>
            <td>{{$user->name}}</td>
          </tr>
          <tr>
            <td class="textt">Tgl. Transaksi</td>
            <td>:</td>
            <td>{{\Carbon\Carbon::parse($transaksi->created_at)->isoFormat('D MMMM Y')}}</td>
          </tr>
          <tr>
            <td class="textt">No. Transaksi</td>
            <td>:</td>
            <td>{{$transaksi->kode_transaksi}}</td>
          </tr>
          </tbody>
        </table>
      </div>
    </div><!--End Invoice Mid-->
 
    <div id="bot">
      <div id="table">
          <table cellspacing='0' cellpadding='0' style='width:218px; font-size:12px; font-family:Courier New;  border-collapse: collapse; margin-top: 5px;' border='0'>
              <tr align='center'>
                  <td width='14%'><b>Item</b></td>
                  <td width='16%'><b>Harga</b></td>
                  <td width='4%'><b>Qty</b></td>
                  <td width='13%'><b>Jumlah</b></td><tr>
                  <td colspan='5'><hr></td></tr>
              </tr>
              @foreach($keranjang as $cart)
                  <tr>
                      <td style='vertical-align:center; padding-right:10px'>{{$cart->nama_barang}}</td>
                      <td style='vertical-align:center; text-align:right; padding-right:10px;'>{{number_format($cart->harga,0,',','.')}}</td>
                      <td style='vertical-align:center; text-align:right; padding-right:10px'>{{$cart->qty}}</td>
                      <td style='text-align:right; vertical-align:center'>{{number_format($cart->jumlah,0,',','.')}}</td>
                  </tr>
              @endforeach
              <tr>
                  <td colspan='5'><hr></td>
              </tr>
              <tr style="font-size: 14px; color:black">
                  <td colspan = '1'><div style='text-align:left;'><b>Total</b></div></td>
                  <td colspan = '1'><div style='text-align:left;'><b>:</b></div></td>
                  <td colspan="2" style='text-align:right;'><b>{{ number_format($transaksi->total,0,',','.')}}</b></td>
              </tr>
              <tr style="font-size: 14px; color:black">
                  <td colspan = '1'><div style='text-align:left;'><b>Tunai</b></div></td>
                  <td colspan = '1'><div style='text-align:left;'><b>:</b></div></td>
                  <td colspan="2" style='text-align:right;'><b>{{ number_format($transaksi->jumlah_bayar,0,',','.')}}</b></td>
              </tr>
              <tr style="font-size: 14px; color:black">
                  <td colspan = '1'><div style='text-align:left;'><b>Kembali</b></div></td>
                  <td colspan = '1'><div style='text-align:left;'><b>:</b></div></td>
                  <td colspan="2" style='text-align:right;'><b>{{ number_format($transaksi->jumlah_bayar - $transaksi->total,0,',','.')}}</b></td>
              </tr>
          </table>
          <table style='width:218; font-size:12pt;' cellspacing='2'>
              <tr></br>
                  <td style="text-align: center; font-size: 12px; font-family: Courier New;">****** <b>TERIMA KASIH</b> ******</br></td>
              </tr>
          </table>
      </div><!--End Table-->

      <div style="font-family: Courier New; text-align: center;">
          <p>Barang yang sudah dibeli tidak dapat dikembalikan, Jangan lupa berkunjung kembali.
          </p>
      </div>
    </div>
  </div><!--End Invoice-->
 
</body>
 
</html>