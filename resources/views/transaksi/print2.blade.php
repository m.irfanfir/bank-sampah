<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
</head>
<body style='font-family:tahoma; font-size:8pt;'>
        <center>
            <table style='width:218px; font-size:16pt; font-family:calibri; border-collapse: collapse;' border = '0'>
                <td width='70%' align='CENTER' vertical-align:top'><span style='color:black;'>
                    <b>NOK OUTFIT</b></br>
                    <span style='font-size:12pt;'>No. : {{$transaksi->kode_transaksi}}</span></br>
                </td>
            </table>
        <style>
        hr { 
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        } 
        </style>
        <table cellspacing='0' cellpadding='0' style='width:218px; font-size:12pt; font-family:calibri;  border-collapse: collapse;' border='0'>
            <tr align='center'>
                <td width='14%'>Barang</td>
                <td width='16%'>Harga</td>
                <td width='4%'>Qty</td>
                <td width='13%'>Total</td><tr>
                <td colspan='5'><hr></td></tr>
            </tr>
            @foreach($keranjang as $cart)
                <tr>
                    <td style='vertical-align:top; padding-right:10px'>{{$cart->nama_barang}}</td>
                    <td style='vertical-align:top; text-align:right; padding-right:10px'>{{number_format($cart->harga,0,',','.')}}</td>
                    <td style='vertical-align:top; text-align:right; padding-right:10px'>{{$cart->qty}}</td>
                    <td style='text-align:right; vertical-align:top'>{{number_format($cart->jumlah,0,',','.')}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan='4'><hr></td>
            </tr>
            <tr>
                <td colspan = '1'><div style='text-align:left; color:black'><b>Total : </b></div></td>
                <td colspan="3" style='text-align:right; font-size:12pt; color:black'><b> Rp. {{ number_format($transaksi->total,0,',','.')}}</b></td>
            </tr>
        </table>
        <table style='width:218; font-size:12pt;' cellspacing='2'>
            <tr></br>
                <td align='center'>****** TERIMA KASIH ******</br></td>
            </tr>
        </table>
        </center>
        </body>
        </html>
</body>
</html>