@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>DataTables</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DataTables</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- left column -->
      <div class="col-md-4">
        <!-- general form elements -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Barang</h3>
          </div>
          <form action="{{ url('/add/keranjang')}}" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Barang</label>
                <select class="form-control select2bs4" name="barang" required id="barang">
                    <option value="">Pilih Barang</option>
                    @foreach ($barang as $br)
                        <option value="{{$br->id}}">{{$br->nama_barang}}</option>
                    @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Harga</label>
                <input type="text" class="form-control" id="harga" name="harga" readonly placeholder="Harga">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Qty</label>
                <input type="number" class="form-control" id="qty" min="1" step="1" value="1" required name="qty" placeholder="Qty">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>

      </div>
      <!--/.col (left) -->
      <!-- right column -->
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Keranjang</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="keranjang" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Nama Barang</th>
                <th>Qty</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                @foreach($keranjang as $index => $cart)
                  <tr>
                    <td>{{$index+1}}</td>
                    <td>{{$cart->nama_barang}}</td>
                    <td>{{$cart->qty}}</td>
                    <td>Rp. {{number_format($cart->harga,0,',','.')}}</td>
                    <td>Rp. {{number_format($cart->jumlah,0,',','.')}}</td>
                    <td>
                      <a href="{{ url('/keranjang/delete/'.$cart->id)}}" title="Delete" class="btn btn-default btn-sm button delete-keranjang"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
              <tr>
                <th colspan="6">
                  <div class="row">
                      <div class="col-md-4" style="text-align: left;">
                        <form action="{{ url('/add/transaksi')}}" method="POST" enctype="multipart/form-data">
                          <div class="form-group">
                            <input type="text" class="form-control" id="jml_bayar" required name="jml_bayar" value="0" placeholder="Jumlah Bayar">
                          </div>
                          <input type="hidden" name="_token" value="{{csrf_token()}}">
                          <input type="hidden" value="{{$total}}" name="total">
                          <p id="note" style="font-size: 12px; margin-bottom: 5px; color: red;">*Jumlah bayar kurang</p>
                          <p id="keranjang_kosong" style="font-size: 12px; margin-bottom: 5px; color: red;">*Keranjang masih kosong</p>
                          <button type="submit" id="submit" class="btn btn-primary btn-sm">Submit</button>
                        </form>
                      </div>
                    <div class="col-md-8" style="text-align: right;">
                      Total : Rp. {{number_format($total,0,',','.')}}
                      <input type="hidden" value="{{$total}}" id="total">
                    </div>
                  </div>
                </th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Transaksi</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="transaksi" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Kode Transaksi</th>
                <th>Total</th>
                <th>Action</th>
              </tr>
              </thead>
              @foreach($transaksi as $index => $t)
                <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$t->kode_transaksi}}</td>
                  <td>Rp. {{number_format($t->total,0,',','.')}}</td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm " data-toggle="dropdown">
                        <span class="sr-only">Toggle Dropdown</span><i class="fa-solid fa-gear"></i>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="{{ url('/print/'.$t->id)}}" target="_blank">Print</a>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#detail_transaksi-{{ $t->id }}" target="_blank">Detail Transaksi</a>
                      </div>
                    </div>
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
@include('transaksi.modal')
@stop
@section('jscustom')
<script>
  $('#jml_bayar').val("");
  var rupiah = document.getElementById('jml_bayar');
      rupiah.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        rupiah.value = formatRupiah(this.value, 'Rp. ');
      });
      /* Fungsi formatRupiah */
      function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split       = number_string.split(','),
        sisa        = split[0].length % 3,
        rupiah      = split[0].substr(0, sisa),
        ribuan      = split[0].substr(sisa).match(/\d{3}/gi);
   
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }
   
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
      }
</script>
<script>
  $('#note').hide();
  $('#submit').prop('disabled',true);
  $('#keranjang_kosong').hide();
  var total_awal = $("#total").val()
  $(document).ready(function(){
    $("#jml_bayar").keyup(function(){
      var jml_bayar = $("#jml_bayar").val();
      var bayar = jml_bayar.replaceAll('.', '')
      var total = $("#total").val();
      var number_bayar = Number(bayar);
      var number_total = Number(total);

      if(total_awal == 0){
        $('#submit').prop('disabled',true);
        $('#keranjang_kosong').show();
      }else{
        $('#keranjang_kosong').hide();
        if(number_bayar <= number_total){
          $('#submit').prop('disabled',true);
          $('#note').show();
        }else{
          $('#note').hide();
          $('#submit').prop('disabled',false);
        }
      }
    });
  });
</script>
<script>
    $('#barang').on('change', function(e) {
        cekHarga()
    });

    var parambarang = {
            barang : null
        };

    function cekHarga () {
        parambarang.barang = $('#barang').val();

        $.get('{{url("/cek/harga")}}', parambarang, function(data){
            console.log(data.harga.harga);
            $('#harga').val(new Intl.NumberFormat().format(data.harga.harga));
        })
    }
</script>
<script>
  $(function () {
    $("#keranjang").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    })
  });

  $(function () {
    $("#transaksi").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    })
  });
</script>
<script>
  $('.delete-keranjang').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Data akan dihapus?',
          text: '',
          icon: 'warning',
          buttons: ["Tidak", "Ya!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }else{
            swal("Delete dibatalkan", "", "error");
          }
      });
  });
</script>
<script>
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
</script>
@endsection