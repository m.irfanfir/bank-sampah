@extends('layouts.backend')
@section('content')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <!-- <div class="col-sm-6">
        <h1>DataTables</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">DataTables</li>
        </ol>
      </div> -->
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Names</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <p>
              <a title="Tambah" type="button" href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#tambah_names"><i class="fas fa-plus"></i></a>
              <a href="{{ url('/names/export-pdf')}}" title="Export PDF" class="btn btn-default btn-sm button"><i class="fas fa-file"></i></a>
            </p>
            <form role="form">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Blok</label>
                    <select class="form-control select2bs4" name="alamat" id="alamat">
                        <option value="">Pilih Blok</option>
                        @foreach ($blok as $bl)
                            <option value="{{$bl->id}}" {{$bl->id == $alamat ? "selected" : null}}>{{$bl->blok}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="nama" autocomplete="off" name="nama" value="{{$nama}}" placeholder="Nama">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="exampleInputEmail1">&nbsp</label>
                    <button type="submit" class="btn btn-default form-control">Search</button>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-roup">
                    <label for="exampleInputEmail1">&nbsp</label>
                    <a href="{{url('/names/index')}}" class="btn btn-default form-control">Reset</a>
                  </div>
                </div>
              </div>
            </form>
            <table id="names" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No</th>
                <th>Blok</th>
                <th>Nama</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              @foreach($query as $index => $names)
                <tr>
                  <td>{{$index+1}}</td>
                  <td>{{$names->blok}}</td>
                  <td>{{$names->nama}}</td>
                  <td>
                    <a title="Update" type="button" href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#update_names-{{ $names->id }}"><i class="fas fa-pencil"></i></a>
                    <a href="{{ url('/names/delete/'.$names->id)}}" title="Delete" class="btn btn-default btn-sm button delete-names"><i class="fas fa-trash"></i></a>
                  </td>
                </tr>
              @endforeach
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
@include('names.modal')
@stop
@section('jscustom')
<script>
  $(function () {
    $("#names").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "searching": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<script>
  $('.delete-names').on('click', function (event) {
      event.preventDefault();
      const url = $(this).attr('href');
      swal({
          title: 'Data akan dihapus?',
          text: '',
          icon: 'warning',
          buttons: ["Tidak", "Ya!"],
      }).then(function(value) {
          if (value) {
              window.location.href = url;
          }else{
            swal("Delete dibatalkan", "", "error");
          }
      });
  });
</script>
<script>
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })
</script>
@endsection