<div class="modal fade" id="tambah_names">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Names</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ url('/names/insert')}}" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Blok</label>
                <select class="form-control select2bs4" required name="blok" id="blok">
                    <option value="">Pilih Blok</option>
                    @foreach ($blok as $bl)
                        <option value="{{$bl->id}}">{{$bl->blok}}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" autocomplete="off" required>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

@foreach($query as $index => $names)
<div class="modal fade" id="update_names-{{ $names->id }}">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Update Blok</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ url('/names/update/'.$names->id)}}" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Blok</label>
                <select class="form-control select2bs4" required name="blok" id="blok">
                    <option value="">Pilih Blok</option>
                    @foreach ($blok as $bl)
                        <option value="{{$bl->id}}" {{$bl->id == $names->id_blok ? "selected" : null}}>{{$bl->blok}}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$names->nama}}" id="nama" placeholder="Nama" autocomplete="off" required>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endforeach