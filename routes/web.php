<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true ]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register');

Route::group([ 'middleware' => 'auth' ], function() {
    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'Dashboard'])->name('dashboard');

    // Route::get('/barang', [App\Http\Controllers\BarangController::class, 'Barang'])->name('barang');
    
    // Route::post('/update/{id}', [App\Http\Controllers\BarangController::class, 'Update'])->name('update');
    // Route::post('/add', [App\Http\Controllers\BarangController::class, 'Add'])->name('add');
    // Route::get('/hapus-barang/{id}', [App\Http\Controllers\BarangController::class, 'Delete'])->name('delete');
    // Route::post('/beli/{id}', [App\Http\Controllers\BarangController::class, 'beli'])->name('beli');

    // Route::get('/lang', [App\Http\Controllers\BarangController::class, 'lang'])->name('lang');


//blok
// Route::get('/blok/index', [App\Http\Controllers\BlokController::class, 'index'])->name('index');
// Route::post('/blok/insert', [App\Http\Controllers\BlokController::class, 'insert'])->name('insert');
// Route::post('/blok/update/{id}', [App\Http\Controllers\BlokController::class, 'update'])->name('update');
// Route::get('/blok/delete/{id}', [App\Http\Controllers\BlokController::class, 'delete'])->name('delete');

// Route::get('/names/index', [App\Http\Controllers\NamesController::class, 'index'])->name('names');
// Route::post('/names/insert', [App\Http\Controllers\NamesController::class, 'insert'])->name('insert');
// Route::post('/names/update/{id}', [App\Http\Controllers\NamesController::class, 'update'])->name('update');
// Route::get('/names/delete/{id}', [App\Http\Controllers\NamesController::class, 'delete'])->name('delete');
// Route::get('/names/export-pdf', [App\Http\Controllers\NamesController::class, 'pdf'])->name('pdf');

// Route::get('/transaksi/index', [App\Http\Controllers\TransaksiController::class, 'index'])->name('transaksi');
// Route::get('/cek/harga', [App\Http\Controllers\TransaksiController::class, 'cek_harga'])->name('cek_harga');
// Route::post('/add/keranjang', [App\Http\Controllers\TransaksiController::class, 'add_keranjang'])->name('add_keranjang');
// Route::get('/keranjang/delete/{id}', [App\Http\Controllers\TransaksiController::class, 'keranjang_delete'])->name('keranjang_delete');
// Route::post('/add/transaksi', [App\Http\Controllers\TransaksiController::class, 'add_transaksi'])->name('add_transaksi');
// Route::get('/print/{id}', [App\Http\Controllers\TransaksiController::class, 'print'])->name('print');


// Route::get('/menus', [App\Http\Controllers\MenuController::class, 'index'])->name('menu');

// Route::get('/menus-show', [App\Http\Controllers\MenuController::class, 'show']);
// Route::post('/menus', [App\Http\Controllers\MenuController::class, 'store'])->name('menus.store');

// Route::get('/tiket', [App\Http\Controllers\TiketController::class, 'index'])->name('tiket');
// Route::post('/booking/tiket', [App\Http\Controllers\TiketController::class, 'booking'])->name('tiket');
// Route::post('/import', [App\Http\Controllers\TiketController::class, 'import'])->name('tiket');

//master jenis sampah
Route::get('/jenis-sampah', [App\Http\Controllers\JenisSampahController::class, 'index'])->name('jenis');
Route::post('/jenis-sampah/insert', [App\Http\Controllers\JenisSampahController::class, 'insert'])->name('jenis');
Route::post('/jenis-sampah/update/{id}', [App\Http\Controllers\JenisSampahController::class, 'update'])->name('jenis');
Route::get('/jenis-sampah/delete/{id}', [App\Http\Controllers\JenisSampahController::class, 'delete'])->name('delete');

//master sampah
Route::get('/sampah', [App\Http\Controllers\SampahController::class, 'index'])->name('sampah');
Route::post('/sampah/insert', [App\Http\Controllers\SampahController::class, 'insert'])->name('sampah');
Route::post('/sampah/update/{id}', [App\Http\Controllers\SampahController::class, 'update'])->name('sampah');
Route::get('/sampah/delete/{id}', [App\Http\Controllers\SampahController::class, 'delete'])->name('sampah');

//kalkulator sampah
Route::get('/kalkulator-sampah', [App\Http\Controllers\KalkulatorSampahController::class, 'index'])->name('kalkulator');
Route::get('/cek/harga', [App\Http\Controllers\KalkulatorSampahController::class, 'cek_harga'])->name('cek_harga');

Route::post('/transaksi', [App\Http\Controllers\TransaksiSampahController::class, 'transaksi'])->name('transaksi');

});
